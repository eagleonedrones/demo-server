import asyncio

from utils.aiotools import run_in_sync

from initiate import get_fixtures, reference_drones_out_of_fixtures, initiate_drones, create_hub_entities, \
    remove_all_hub_entities


async def startup():
    fixtures = get_fixtures()
    drones = reference_drones_out_of_fixtures(fixtures)
    initiate_drones(drones)
    remove_all_hub_entities()
    create_hub_entities(fixtures)

    # connect all initiated drones
    await asyncio.gather(*[
        drone['instance'].connect()
        for drone in drones
    ])


if __name__ == '__main__':
    run_in_sync(startup)()
