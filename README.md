# Demo server for testing purposes

All demo data can be found in `/fixtures` folder and it's JSON files.


## How it works

1. creates all entities defined at `/fixtures/entities.json`,
2. created Eagle entities are therefore connected to HUB,
3. Eagle entities are imitating real Eagle behavior, such as
   replies for state changes and periodical updates of state
   and current location. 
