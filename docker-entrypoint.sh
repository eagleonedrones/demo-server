#!/bin/bash

set -e

# Wait until Backoffice is up
until wget http://nginx/admin/ -O - > /dev/null; do
  echo "Backoffice is unavailable - sleeping"
  sleep 1
done

echo "Backoffice is up - executing command"
# Commands available using `docker-compose run demo_server [COMMAND]`
case "$1" in
    python)
        /venv/bin/python
    ;;
    test)
        /venv/bin/python test.py
    ;;
    *)
        echo "Running Production Server..."
        /venv/bin/python runserver.py
    ;;
esac
