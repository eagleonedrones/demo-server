import asyncio
import copy
import json
import os

from client.client import HubClient
from client.input_formats import MessageFromHub
from utils.crypting import get_rsa_keys, export_key_pub

from contrib import RestServiceProxy

DRONE_STATUS_INTERVAL = 1
BASE_DIR = os.environ['BASE_DIR']
HUB_HOST = os.environ['HUB_HOST']
HUB_PORT = os.environ['HUB_PORT']
HUB_WS_URL = f"ws://{HUB_HOST}:{HUB_PORT}/ws"

HUB_KEY_PUB, _ = get_rsa_keys(pair_name='hub')

BACKOFFICE_HOST = os.environ['BACKOFFICE_HOST']
BACKOFFICE_PORT = os.environ['BACKOFFICE_PORT']

BACKOFFICE_USER_USERNAME = os.environ['BACKOFFICE_USER_USERNAME']
BACKOFFICE_USER_PASSWORD = os.environ['BACKOFFICE_USER_PASSWORD']

API_BASE = f"http://{BACKOFFICE_HOST}:{BACKOFFICE_PORT}/api/v1/"


_client = None


with open(os.path.join(BASE_DIR, 'fixtures', 'drone-states.json'), 'rb') as f:
    DRONE_STATES = json.loads(f.read())


def rest_client_factory():
    global _client
    if not _client:
        _client = RestServiceProxy(API_BASE, BACKOFFICE_USER_USERNAME, BACKOFFICE_USER_PASSWORD)
    return _client


client = rest_client_factory()


class Drone(HubClient):
    def __init__(self, drone_config):
        drone_slug_name = drone_config['name']
        drone_slug_name = drone_slug_name.lower().replace(' ', '').replace('#', '')
        key_pub, key = get_rsa_keys(pair_name=drone_slug_name)

        super(Drone, self).__init__(HUB_WS_URL, key_pub, key, HUB_KEY_PUB, auto_reconnect=True)

        self.state = copy.deepcopy(DRONE_STATES['standby'])

        self.config = drone_config
        self.config['instance'] = self
        self.config['public_key'] = export_key_pub(key_pub)

        self.pending_state_task = None

    async def set_initial_state(self):
        await self.set_state('standby', {}, force=True)

    async def process_message(self, message: MessageFromHub):
        if message.message_type == 'device/setstate':
            new_state = message.data.plain
            await self.set_state(**new_state)

    async def set_state(self, name, params, delayed=0, force=False):
        await asyncio.sleep(delayed)

        if force or name in self.state['valid_states']:
            if self.pending_state_task:
                try:
                    self.pending_state_task.cancel()
                    await self.pending_state_task
                except asyncio.CancelledError:
                    pass

            prev_state = copy.deepcopy(self.state)
            new_state = copy.deepcopy(DRONE_STATES[name])
            self.state.update(new_state)
            self.state['current_state']['params'].update(params)

            if name in {'missionsucceed', 'missionfailed'}:
                current_action = params.get('current_action', None)
                if current_action:
                    if force or current_action in self.state['current_state']['params']['valid_actions']:
                        self.state['current_state']['params']['current_action'] = current_action
                        self.state['current_state']['params']['valid_actions'].remove(current_action)
                    else:
                        self.state = prev_state

            if name == 'actionneeded' and prev_state['current_state']['name'] == 'actionneeded':
                if not self.state['netgun_ready']:
                    self.state['netgun_ready'] = True
                else:
                    self.state['battery']['charging'] = True

            # pass the state to all listeners
            await self._send_state()

            #
            # following actions are not triggered by the pilot

            # automatically start to follow target
            if name in {'hunter', 'radar', 'manualcontrol'}:
                self.pending_state_task = asyncio.create_task(
                    self.set_state('followingtarget', {'target_distance': 15.2}, delayed=3, force=True))

            # catch the target
            if name == 'followingtarget':
                self.pending_state_task = asyncio.create_task(
                    self.set_state('missionsucceed', {}, delayed=3, force=True))

            # land somewhere when no action is taken
            if name in {'missionsucceed', 'missionfailed'}:
                self.pending_state_task = asyncio.create_task(
                    self.set_state('actionneeded', {}, delayed=2, force=True))

            # landed, waiting for making netgun ready
            if name == 'actionneeded':
                if not self.state['netgun_ready']:
                    self.pending_state_task = asyncio.create_task(
                        self.set_state('actionneeded', {}, delayed=1, force=True))

                # netgun ready, waiting to be charged
                elif not self.state['battery']['charging']:
                    self.pending_state_task = asyncio.create_task(
                        self.set_state('actionneeded', {}, delayed=1, force=True))

                # now is being charged, going to ready state again
                else:
                    self.pending_state_task = asyncio.create_task(
                        self.set_state('standby', {}, delayed=3, force=True))

        # else: - not implemented
        #
        # we won't implement error responses to `sender_id` - it could be easily
        # exploited to discover existing locations and their devices by sending
        # invalid `device/setstate` to random `location_id`

    async def _send_state(self):
        await self.send(message_type='device/info', data=self.state, location=self.config['location_id'])

    async def _send_location(self):
        self.location = {
            "lat": 234.234,
            "lng": 223.112,
            "alt": 15.1
        }
        await self.send(message_type='device/location', data=self.location, location=self.config['location_id'])

    async def state_and_location_sender(self):
        # while True:
        #     await self._send_state()
        #     await self._send_location()
        #     await asyncio.sleep(DRONE_STATUS_INTERVAL)
        pass

    async def message_processor(self):
        async for message in self:
            await self.process_message(message)

    async def connect(self, silent_disconnect=False):
        await asyncio.gather(super(Drone, self).connect(silent_disconnect),
                             self.state_and_location_sender(),
                             self.message_processor())


def reference_drones_out_of_fixtures(fixtures):
    drones = []
    for company in fixtures['companies']:
        for location in company['locations']:
            for device in location['devices']:
                drones.append(device)

    return drones


def initiate_drones(drones):
    for drone in drones:
        Drone(drone)


def create_hub_entities(fixtures):
    for _c, company in enumerate(fixtures['companies']):
        members = company.pop('members')
        locations = company.pop('locations')

        company = client.post('/companies/', json=company).json()
        company['members'] = members
        company['locations'] = locations
        fixtures['companies'][_c] = company

        for member in members:
            client.post(f"/companies/{company['id']}/members/", json=member)

        for _l, location in enumerate(locations):
            devices = location.pop('devices')

            location.update({'company_id': company['id']})
            location = client.post('/locations/', json=location).json()
            location['devices'] = devices
            locations[_l] = location

            for device in devices:
                device.update({'location_id': location['id']})
                _device = device.copy()
                del _device['instance']
                _device = client.post('/devices/', json=_device).json()
                device.update(_device)


def remove_all_hub_entities():
    while True:
        devices = client.get('/devices/').json()['results']
        if not devices:
            break

        for device in devices:
            assert 204 == client.delete(f"/devices/{device['id']}").status_code

    while True:
        locations = client.get('/locations/').json()['results']
        if not locations:
            break

        for location in locations:
            assert 204 == client.delete(f"/locations/{location['id']}").status_code

    while True:
        companies = client.get('/companies/').json()['results']
        if not companies:
            break

        for company in companies:
            while True:
                members = client.get(f"/companies/{company['id']}/members/").json()['results']
                if not members:
                    break

                for member in members:
                    assert 204 == client.delete(f"/companies/{company['id']}/members/{member['id']}/").status_code

            assert 204 == client.delete(f"/companies/{company['id']}").status_code


def get_fixtures():
    with open(os.path.join(BASE_DIR, 'fixtures', 'entities.json'), 'rb') as f:
        return json.loads(f.read())
