import requests


class RestServiceProxy(object):
    def __init__(self, base_url, username, password):
        self.base_url = base_url.rstrip('/')
        self.auth = (username, password)
        self.client = requests.session()

        self.token = self._get_token()
        self.client.headers['Authorization'] = 'Token {}'.format(self.token)
        self.client.headers['Accept'] = 'application/json'

    def _request(self, method, endpoint, **kwargs):
        url = '{base_url}/{endpoint}/'.format(base_url=self.base_url, endpoint=endpoint.strip('/'))
        headers = self._token_header
        headers.update(kwargs.pop('headers', {}))

        return getattr(self.client, method)(url, headers=headers, **kwargs)

    def get(self, endpoint, **kwargs):
        return self._request('get', endpoint, **kwargs)

    def post(self, endpoint, **kwargs):
        return self._request('post', endpoint, **kwargs)

    def put(self, endpoint, **kwargs):
        return self._request('put', endpoint, **kwargs)

    def delete(self, endpoint, **kwargs):
        return self._request('delete', endpoint, **kwargs)

    def _get_token(self):
        url = '{base_url}/token/'.format(base_url=self.base_url)
        request = requests.get(url, auth=self.auth)
        response = request.json()
        return response['token']

    @property
    def _token_header(self):
        return {'Authorization': 'Token {token}'.format(token=self.token)}
