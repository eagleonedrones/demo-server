import asyncio
import os

from client.client import HubClient
from client.responses import MESSAGE_CONFIRMATION
from utils.aiotools import sleep_until
from utils.crypting import get_rsa_keys, export_key_pub

from initiate import get_fixtures, reference_drones_out_of_fixtures, initiate_drones, create_hub_entities, \
    rest_client_factory, remove_all_hub_entities

HUB_HOST = os.environ['HUB_HOST']
HUB_PORT = os.environ['HUB_PORT']
HUB_WS_URL = f"ws://{HUB_HOST}:{HUB_PORT}/ws"

HUB_KEY_PUB, _ = get_rsa_keys(pair_name='hub')

client = rest_client_factory()


class MobileAppClient(HubClient):
    def __init__(self, user_config):
        user_email = user_config['email']

        # create RSA keys
        dot_email = user_email.replace('@', '.')
        key_pub, key = get_rsa_keys(pair_name=dot_email)

        # register with user sessions
        client.post(f"/users/{user_email}/sessions/", json={
            'public_key': export_key_pub(key_pub),
        })

        super(MobileAppClient, self).__init__(HUB_WS_URL, key_pub, key, HUB_KEY_PUB, auto_reconnect=True)


def reference_members_out_of_fixtures(fixtures):
    members = {}
    for company in fixtures['companies']:
        for member in company['members']:
            members.setdefault(member['email'], [])
            members[member['email']].append(member)

    return members.values()


def initiate_members(members):
    for member_roles in members:
        instance = MobileAppClient(member_roles[0])
        for member_role in member_roles:
            member_role['instance'] = instance


async def run_tests(fixtures, drones):
    await test_multiple_entity_interaction_scenario(fixtures)

    company = fixtures['companies'][0]
    member, drone = company['members'][0]['instance'], company['locations'][0]['devices'][0]['instance']

    # get all drones back to initial state
    for _drone in drones:
        await _drone['instance'].set_initial_state()

    await test_app_lifecycle_scenario(drone, member)

    # test the drone can run whole lifecycle multiple times
    await test_app_lifecycle_scenario(drone, member)
    await test_app_lifecycle_scenario(drone, member)

    # halt all parallel loops etc.
    print('Halting all running loops...')
    cancelled_count = 0
    for pending_task in asyncio.Task.all_tasks():
        try:
            pending_task.cancel()
            await pending_task

        # some Websockets ends up with AssertionError (`/protocol.py#L431`)
        except (asyncio.CancelledError, AssertionError):
            cancelled_count += 1

    print(f"{cancelled_count} tasks cancelled")


async def startup():
    fixtures = get_fixtures()
    drones = reference_drones_out_of_fixtures(fixtures)
    initiate_drones(drones)
    remove_all_hub_entities()
    create_hub_entities(fixtures)

    # as members can have multiple Public Keys (device_ids), we will
    # register their devices later (MC - Mobile app Client)
    members = reference_members_out_of_fixtures(fixtures)
    unique_members = [_m[0] for _m in members]
    initiate_members(members)

    # connect all initiated devices (drones and MCs)
    await asyncio.gather(*[
        entity['instance'].connect()
        for entity in drones + unique_members
    ], run_tests(fixtures, drones))


async def _test_received_state_change_message(drone, member, test_state):
    received_states = []
    checking_errors = []

    def _test(state, _test_state):
        for key, value in _test_state.items():
            if type(value) is dict:
                _test(state[key], value)
            else:
                assert state[key] == value

    async def _wait_for_new_state_update():
        async for message in member:
            if message.message_type == 'device/info' and message.sender_id == drone.device_id:
                received_states.append(message.data)
                try:
                    _test(message.data.plain, test_state)
                    return

                except AssertionError as e:
                    checking_errors.append(e)

    done, pending = await asyncio.wait([_wait_for_new_state_update()], timeout=10)
    if pending:
        print('timeouted')
        print(test_state)
        print(drone.state)
        print(received_states[-1])
        raise TimeoutError

    return received_states


async def test_multiple_entity_interaction_scenario(fixtures):
    #####################
    #                   #
    #   TEST SCENARIO   #
    #                   #
    #####################

    # initiates all company members defined at `fixtures/entities.json`
    # and tests all possible interactions between themselves
    #
    print('Running `multiple entity interaction` scenario')

    drones = reference_drones_out_of_fixtures(fixtures)
    members = reference_members_out_of_fixtures(fixtures)
    unique_members = [_m[0] for _m in members]

    await sleep_until(lambda: all(bool(e['instance'].websocket) for e in drones + unique_members))
    await asyncio.sleep(2)

    # all MCs send `set_state` to all drones
    # they should receive replies only from drones within their companies
    for member in unique_members:
        for drone in drones:
            await member['instance'].send(message_type='device/setstate',
                                          data={
                                              "name": "hunter", "params": {}
                                          },
                                          receiver_id=drone['instance'].device_id)

    await sleep_until(lambda: len(drones[-1]['instance'].received_messages))
    await asyncio.sleep(1)

    # TODO: test že posílají zprávy v nějakém intervalu samy od sebe
    # TODO: (tj. počkat ten interval na další zprávu)

    # test all MCs have sent messages
    for member_roles in members:
        for member_role in member_roles:
            assert len(drones) == len(member_role['instance'].sent_messages)

    # test all drones received all messages
    for drone in drones:
        # messages from mobile users
        filtered_messages = list(filter(
            lambda msg: msg.message_type == 'device/setstate',
            drone['instance'].received_messages))

        assert len(unique_members) == len(filtered_messages)

        # messages form other drones except the drone itself
        filtered_messages = list(filter(
            lambda msg: msg.message_type == 'device/info',
            drone['instance'].received_messages))

        local_drones = list(filter(
            lambda _d: _d['location_id'] == drone['location_id'],
            drones))

        assert len(local_drones) - 1 == len(filtered_messages)

        # there're no other messages
        filtered_messages = list(filter(
            lambda msg: msg.message_type not in {'device/setstate', 'device/info', MESSAGE_CONFIRMATION},
            drone['instance'].received_messages))

        assert not filtered_messages

    # test MCs received messages from drones from their companies
    for company in fixtures['companies']:
        def _flatten(l):
            return [item for sublist in l for item in sublist]

        c_drone_ids = {
            drone['instance'].device_id
            for drone in _flatten([
                # locations
                location['devices']
                for location in company['locations']
            ])
        }
        for member in company['members']:
            # each company member have received messages from all company drones
            # and their `sender_id` match exactly with company drones
            filtered_messages = list(filter(
                lambda msg: msg.sender_id in c_drone_ids,
                member['instance'].received_messages))

            assert len(c_drone_ids) == len(filtered_messages)

    print('All `multiple entity interaction` scenario tests have successfully passed')


async def test_app_lifecycle_scenario(drone, member):
    # Pick one initiated member and one initiated drone from a single company and guide
    # that drone through all app life-cycle states.
    #
    print('Running `app lifecycle` scenario')

    print('member.device_id', member.device_id)
    print('drone.device_id', drone.device_id)

    assert drone.state['current_state']['name'] == 'standby'
    assert drone.state['valid_states'] == ['hunter', 'radar']
    assert drone.state['battery']['charging']

    # set radar mode with initial params
    params = {'altitude': 10, 'perimeter': 50}
    await member.send('device/setstate', {
        'name': 'radar',
        'params': params}, receiver_id=drone.device_id)
    await _test_received_state_change_message(drone, member, {
        'battery': {
            'charging': False
        },
        'current_state': {
            'name': 'radar',
            'params': params}
    })

    # alter radar mode params and update
    params['altitude'] = 20
    await member.send('device/setstate', {
        'name': 'radar',
        'params': params}, receiver_id=drone.device_id)
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'radar',
            'params': params}
    })

    # change mode to hunter (with no params)
    await member.send('device/setstate', {
        'name': 'hunter',
        'params': {}}, receiver_id=drone.device_id)
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'hunter',
            'params': {}}
    })

    # drone should after 5s automatically start following target
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'followingtarget',
            'params': {'target_distance': 15.2}}
    })

    # drone should finish mission successfully within next 5s
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'missionsucceed',
            'params': {
                'current_action': None,
                'valid_actions': [
                    'land_home',
                    'land_startpoint',
                    'land_immediately',
                ]
            }}
    })

    # set 1st option after missionsucceed
    params = {'current_action': 'land_home'}
    await member.send('device/setstate', {
        'name': 'missionsucceed',
        'params': params}, receiver_id=drone.device_id)
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'missionsucceed',
            'params': {
                'current_action': 'land_home',
                'valid_actions': [
                    'land_startpoint',
                    'land_immediately',
                ]
            }}
    })

    # set 2st option after missionsucceed
    params = {'current_action': 'land_startpoint'}
    await member.send('device/setstate', {
        'name': 'missionsucceed',
        'params': params}, receiver_id=drone.device_id)
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'missionsucceed',
            'params': {
                'current_action': 'land_startpoint',
                'valid_actions': [
                    'land_home',
                    'land_immediately',
                ]
            }}
    })

    # drone should appear in actioneeded state
    await _test_received_state_change_message(drone, member, {
        'battery': {
            'charging': False
        },
        'netgun_ready': False,
        'current_state': {
            'name': 'actionneeded',
            'params': {}}
    })

    # netgun is ready
    await _test_received_state_change_message(drone, member, {
        'battery': {
            'charging': False
        },
        'netgun_ready': True,
        'current_state': {
            'name': 'actionneeded',
            'params': {}}
    })

    # drone is charging
    await _test_received_state_change_message(drone, member, {
        'battery': {
            'charging': True
        },
        'netgun_ready': True,
        'current_state': {
            'name': 'actionneeded',
            'params': {}}
    })

    # drone is at standby again
    await _test_received_state_change_message(drone, member, {
        'current_state': {
            'name': 'standby',
        }
    })

    # TODO: actionneeded battery, net, standby

    print('All `app lifecycle` scenario tests have successfully passed')


if __name__ == '__main__':
    try:
        asyncio.run(startup(), debug=True)
    except asyncio.CancelledError:
        print('finished...')
